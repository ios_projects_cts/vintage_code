//
//  MapViewController.swift
//  Vintage
//
//  Created by chawtech solutions on 3/22/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController,GMSMapViewDelegate {
  @IBOutlet weak var mapView : GMSMapView!
     let marker = GMSMarker()
    var lat = Double()
    var camera = GMSCameraPosition()
     var lng = Double()
    var address = String()
    var address_image = String()
    var markerChoose = Bool()
    var mapListArr = NSMutableArray()

    var activityIndicatorView: ActivityIndicatorView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Map"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backItem?.title = ""
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        markerChoose = false
        
        if address_image == "google"
        {
           
            
            
            
            self.callgetMapToueListMethod()
            
//            let mark = GMSMarker()
//            mark.position = CLLocationCoordinate2D(latitude: 51.52306747, longitude: -0.10392300)
//            //  self.marker.title = (addressDict["Name"] as? String)!
//            mark.title = "Zetter Townhouse Clerkenwell"
//            mark.isDraggable = false
//            mark.icon = UIImage.init(named: "marker")
//            
//            mark.infoWindowAnchor = CGPoint(x:0.5, y:0.2)
//            mark.map = self.mapView
//             mapView.delegate = nil
            
            
           
        }
       
        else
        {
        camera = GMSCameraPosition.camera(withLatitude: lat,longitude:lng, zoom: 20)
        
        mapView.camera = camera
        mapView.isMyLocationEnabled = false
         mapView.delegate = self
        self.marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        //  self.marker.title = (addressDict["Name"] as? String)!
       // self.marker.title = "Central London,London,UK"
        self.marker.isDraggable = false
        self.marker.infoWindowAnchor = CGPoint(x:0.5, y:0.2)
        self.marker.map = self.mapView
        self.mapView.selectedMarker = self.marker
        }
        
        
//        // get formatted address of current user from geolocation
//        let geoCoder = CLGeocoder()
//        let location = CLLocation(latitude: lat, longitude: lng)
//        
//        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
//            guard let addressDict = placemarks?[0].addressDictionary else {
//                return
//            }
//            
//            
//            //  print(addressDict)
//            //Print each key-value pair in a new row
//            //    addressDict.forEach { print($0) }
//            
//            
//            // Print fully formatted address
//            if let formattedAddress = addressDict["FormattedAddressLines"] as? [String] {
//                print(formattedAddress.joined(separator: ", "))
//                self.address = formattedAddress.joined(separator: ", ")
//                
//                
//            }
//        })

        let share_btn = UIButton(type: .custom)
        share_btn.setImage(UIImage(named: "share"), for: .normal)
        share_btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        share_btn.addTarget(self, action: #selector(TourDetailViewController.clickOnShareMethod), for: .touchUpInside)
        let share_btnn = UIBarButtonItem(customView: share_btn)
        
        self.navigationItem.setRightBarButtonItems([share_btnn], animated: true)
        
        
    }
    
    
    func clickOnShareMethod(sender: UIBarButtonItem)
    {
        
       var text = String()
        if address_image == "google"
        {
            for i in 0...mapListArr.count-1
            {
                let Object = mapListArr[i] as! [String : AnyObject]
                let title =  (Object["title"] as? String)
                if text.isEmpty
                {
                     text =  "Locations - "  + title!
                }
                else
                {
                text = text + "," + title!
                }
            }
        }
        else
        {
                // text to share
                let address = "Name - " + self.address
                text =  address
        }
        
                // set up activity view controller
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
                // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        
                // present the view controller
                self.present(activityViewController, animated: true, completion: nil)
                
        
    }

    
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        // 1
      //  let a:Int! = Int(marker.accessibilityLabel!)
        // 2
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
       // customInfoWindow.architectLbl.text = "Comimg Soon"
        customInfoWindow.completedYearLbl.text = address
        DispatchQueue.main.async {
            if self.address_image == "google"
            {
                customInfoWindow.imageview.image = UIImage.init(named: self.address_image)
            }
            else
            {
            let imgURL = URL(string:self.address_image)
            customInfoWindow.imageview.kf.setImage(with:imgURL)
            }
            customInfoWindow.imageview.layer.cornerRadius = customInfoWindow.imageview.frame.size.height/2
            customInfoWindow.imageview.clipsToBounds = true
        }

        return customInfoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
         self.navigationController?.popViewController(animated: true)
        return false
    }
    
    // MARK:- Server call of getService Provider Method
    func callgetMapToueListMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        let string_url = kbaseUrl + kadditionalgetmapTourListUrl
        
        let url = URL(string: string_url)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            // let json = try! JSONSerialization.jsonObject(with: data, options: [])
            let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            
            print(json as Any)
            // parse data from server
            let careerArr = json?["data"] as! NSArray
            if careerArr.count != 0
            {
                self.parseGetCareerListData(dataArray: careerArr)
            }
            else
            {
                DispatchQueue.main.async {
                self.activityIndicatorView.stopAnimating()
                // UIApplication.shared.beginIgnoringInteractionEvents()
                }
            }
        }
        
        task.resume()
    }
    
    
    //  MARK:- parseGetserviceProviderListData
    func parseGetCareerListData(dataArray:NSArray)
    {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()}
        mapListArr.removeAllObjects()
        let careerDict = NSMutableDictionary()
        let arrJson = dataArray as! Array<Dictionary<String,Any>>
        for i in 0...dataArray.count-1
        {
            let Object = arrJson[i] as [String : AnyObject]
            careerDict.setValue((Object["id"] as? NSString), forKey: "id")
            careerDict.setValue((Object["title"] as? NSString), forKey: "title")
            careerDict.setValue((Object["location"] as? NSString), forKey: "location")
            careerDict.setValue((Object["category_id"] as? NSString), forKey:"category_id")
            careerDict.setValue((Object["lat"] as? NSString), forKey: "lat")
            careerDict.setValue((Object["lng"] as? NSString), forKey: "lng")
            mapListArr.add(careerDict .mutableCopy())
            
        }
        
        
        for i in 0...mapListArr.count-1
        {
            let Object = mapListArr[i] as! [String : AnyObject]
            let lat =  (Object["lat"] as? NSString)?.doubleValue
            let long = (Object["lng"] as? NSString)?.doubleValue
            let latitude =  NSNumber(value:lat!)
            let longitude =  NSNumber(value:long!)
            
            DispatchQueue.main.async {
               
                self.camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude),longitude:CLLocationDegrees(longitude), zoom: 14)
                self.mapView.camera = self.camera
                let position = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
                let marker = GMSMarker(position: position)
                marker.title = (Object["title"] as? String)
                marker.map = self.mapView
                marker.icon = UIImage(named:"marker")
                self.mapView.selectedMarker = marker
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
