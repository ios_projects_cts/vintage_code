//
//  Constant.swift
//  YouTow
//
//  Created by chawtech solutions on 1/17/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

//MARK:- google login client id
var kgoogleClientID = "258313360966-tnc401ovp7anrqkeqi330r6b76q09op4.apps.googleusercontent.com"
var dataBaseName = "Jolt.DB"

//MARK:- server url
var kbaseUrl = "http://35.167.21.236/vintage/"
var kadditionalgetTourListUrl = "tours/tour"
var kadditionalgetTourDetailUrl = "tours/toursdetail"
var kadditionalaboutUsUrl = "cms/aboutus"
var kadditionalContactusUrl = "cms/contactus"
var kadditionalloginUrl = "appusers/login"
var kadditionalforgetPasswordUrl = "appusers/forgetpassword"
var kadditionalRegistrationUrl = "appusers/registration"
var kadditionalupdateNotificationUrl = "appusers/updatenotification"
var kadditionalgetmapTourListUrl = "tours/tourformap"
var kadditionalupdateFcmTokensUrl = "appusers/updatefcm"
var kadditionalupdatepaymentUrl = "customer/update_payment"
var kadditionalgetCategoryListUrl = "tours/getCategoryList"

extension UIViewController {
    
    //MARK:- set alert controller globally
    func displayAlertWithMessage(viewController: UIViewController, title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title:"OK", style: .default, handler: nil))
        viewController.present(ac, animated: true){}
    }
}

class Constant: NSObject {

    
    
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleToFill) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
