//
//  CategoryViewController.swift
//  Vintage
//
//  Created by chawtech solutions on 4/6/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    
      var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet weak var tbl_lettersList: UITableView!
     var letterslistArr = NSMutableArray()
 

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Info Trend Links and Settings"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backItem?.title = ""
//       navigationController?.navigationBar.titleTextAttributes = [
//            NSFontAttributeName: UIFont(name: "Gotham Bold", size: 13)!,NSForegroundColorAttributeName:UIColor.white
//        ]
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addFooter()
        self.callgetCategoryListMethod()
        
        let share_btn = UIButton(type: .custom)
        share_btn.setImage(UIImage(named: "share"), for: .normal)
        share_btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        share_btn.addTarget(self, action: #selector(TourDetailViewController.clickOnShareMethod), for: .touchUpInside)
        let share_btnn = UIBarButtonItem(customView: share_btn)
        
        self.navigationItem.setRightBarButtonItems([share_btnn], animated: true)
        
        
    }
    
    
    func clickOnShareMethod(sender: UIBarButtonItem)
    {
        var  text = String()
        
        // text to share
        
        for i in 0...letterslistArr.count-1
        {
            let Object = letterslistArr[i] as! [String : AnyObject]
            let title =  (Object["name"] as? String)
            if text.isEmpty
            {
                text =  "Categories - "  + title!
            }
            else
            {
                text = text + "," + title!
            }
        }
      
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }

    
    
    // MARK:- Server call of getService Provider Method
    func callgetCategoryListMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        let string_url = kbaseUrl + kadditionalgetCategoryListUrl
        
        let url = URL(string: string_url)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            // let json = try! JSONSerialization.jsonObject(with: data, options: [])
            let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            
            print(json as Any)
            // parse data from server
           
            let Arr = json?["data"] as? [[String: Any]]
           // self.parseGetserviceProviderListData(dataArray: Arr!)
            if Arr?.count != 0
            {
                self.parseGetCategoryListData(dataArray: Arr!)
            }
              
            else
            {
                DispatchQueue.main.async
                {
                    self.activityIndicatorView.stopAnimating()
                }
             }
        }
        
           task.resume()
        }
    
    // MARK:- parseGetserviceProviderListData
    func parseGetCategoryListData(dataArray:[[String: Any]])
    {
        let tourDict = NSMutableDictionary()
        for i in 0...dataArray.count-1
        {
            //tourDict.setValue((dataArray[i]["feature_image"] as? NSString), forKey: "feature_image")
            tourDict.setValue((dataArray[i]["id"] as? NSString), forKey: "id")
            tourDict.setValue((dataArray[i]["name"] as? NSString), forKey: "name")
            tourDict.setValue((dataArray[i]["title"] as? NSString), forKey: "title")
           // tourDict.setValue((dataArray[i]["id"] as? NSString), forKey: "id")
            letterslistArr.add(tourDict .mutableCopy())
            
        }
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                self.tbl_lettersList.reloadData()
                
        }
    }

    func addFooter() {
        let v = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view.frame.size.width), height: CGFloat(0)))
        v.backgroundColor = UIColor.clear
        tbl_lettersList.tableFooterView = v
    }
    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return letterslistArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)
        
        
        
        //  cell.backgroundColor = UIColor.white
        let dict = letterslistArr[indexPath.row] as? Dictionary<String,String>

        cell.textLabel?.text = dict?["name"]
        cell.textLabel?.font  = UIFont(name:"Gotham Bold", size: 16)
        
        
        return cell
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
     //   let category = letterslistArr[indexPath.row]
         let dict = letterslistArr[indexPath.row] as? Dictionary<String,String>
       
         let cat_id = (dict?["id"])! as String
        
//      _ = self.navigationController?.popViewController(animated: true)
        for controller: UIViewController in (self.navigationController?.viewControllers)! {
            if (controller is HomeViewController) {
                
               self.navigationController?.popToViewController(controller, animated: true)
                
            }
        }
        
        let imageDataDict:[String: String] = ["cat":cat_id]
        
        // post a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "categoryprovider"), object: nil, userInfo: imageDataDict)
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
