//
//  TourDetailViewController.swift
//  Vintage
//
//  Created by chawtech solutions on 3/22/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class TourDetailViewController: UIViewController,UIScrollViewDelegate {
    
     @IBOutlet weak var view_back_scroll: UIView!
     var activityIndicatorView: ActivityIndicatorView!
     @IBOutlet weak var view_description : UIView!
     @IBOutlet weak var view_detail : UIView!
     @IBOutlet weak var imgv_image : UIImageView!
     @IBOutlet weak var lbl_title : UILabel!
     @IBOutlet weak var lbl_address : UILabel!
     @IBOutlet weak var lbl_description : UILabel!
     @IBOutlet weak var btn_price : UIButton!
     var tour_id = String()
     var category_id = String()
     var callingWay = String()
     var scrollview_pagecontrol: UIScrollView?
     var pageControl: UIPageControl?
     var bannerImgArr = NSMutableArray()
     var lat = Double()
     var lng = Double()
     var address = String()
     var address_image = String()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "LONDON LETTERING"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
      //  navigationController?.navigationBar.backItem?.title = ""
        
        
    }
       override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view_description.backgroundColor = UIColor.white
        self.view_description.layer.cornerRadius = 5.0
        // self.view_taxiBack.layer.borderWidth = 2.0
        self.view_description.layer.borderColor = UIColor.clear.cgColor
        self.view_description.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_description.layer.shadowOpacity = 1.0
        self.view_description.layer.shadowRadius = 1.0
        self.view_description.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(1))

        
        self.view_detail.backgroundColor = UIColor.white
        self.view_detail.layer.cornerRadius = 5.0
        // self.view_taxiBack.layer.borderWidth = 2.0
        self.view_detail.layer.borderColor = UIColor.clear.cgColor
        self.view_detail.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_detail.layer.shadowOpacity = 1.0
        self.view_detail.layer.shadowRadius = 1.0
        self.view_detail.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(1))

        
        
        let share_btn = UIButton(type: .custom)
        share_btn.setImage(UIImage(named: "share"), for: .normal)
        share_btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        share_btn.addTarget(self, action: #selector(TourDetailViewController.clickOnShareMethod), for: .touchUpInside)
        let share_btnn = UIBarButtonItem(customView: share_btn)
        
        self.navigationItem.setRightBarButtonItems([share_btnn], animated: true)
        self.callgetTourDetailInfoMethod()
       
    }
    func clickOnShareMethod(sender: UIBarButtonItem)
    {
        // text to share
       // let text = "This is some text that I want to share."
         let name = "Name - " + lbl_title.text!
         let address = "Address - " + self.address
        
        let text = name  + address
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    func createPageControl() {
        //scrollview_pagecontrol = UIScrollView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view.frame.size.width), height: CGFloat(217)))
        scrollview_pagecontrol = UIScrollView(frame: CGRect(x: CGFloat(view_back_scroll.frame.origin.x), y: CGFloat(view_back_scroll.frame.origin.y), width: CGFloat(view_back_scroll.frame.size.width), height: CGFloat(self.view_back_scroll.frame.size.height)))
        scrollview_pagecontrol?.showsHorizontalScrollIndicator = false
        scrollview_pagecontrol?.delegate = self
        scrollview_pagecontrol?.bounces = false
        scrollview_pagecontrol?.tag = 1
        self.view_back_scroll.addSubview(scrollview_pagecontrol!)
        pageControl = UIPageControl(frame: CGRect(x: CGFloat(view.frame.size.width / 2.8), y: CGFloat((scrollview_pagecontrol?.frame.size.height)!-20), width: CGFloat(80), height: CGFloat(30)))
        pageControl?.pageIndicatorTintColor = UIColor.lightGray
        pageControl?.numberOfPages = bannerImgArr.count
        pageControl?.currentPage = 0
        pageControl?.currentPageIndicatorTintColor = UIColor.blue
        pageControl?.tag = 12
         self.view_back_scroll.addSubview(pageControl!)
        
        for i in 0..<bannerImgArr.count {
            //We'll create an imageView object in every 'page' of our scrollView.
            var frame = CGRect.zero
            frame.origin.x = view.frame.size.width * CGFloat(i)
            frame.origin.y = 0
            frame.size = view.frame.size
            frame.size.height = (scrollview_pagecontrol?.frame.size.height)!
           // frame.size.width = (scrollview_pagecontrol?.frame.size.height)!
            let imageView = UIImageView(frame: frame)
            let images: String? = (bannerImgArr[i] as? String)
           // imageView.downloadedFrom(link: images!)
            DispatchQueue.main.async {
                let imgURL = URL(string:images!)
                  imageView.kf.setImage(with:imgURL)
            }
          
            scrollview_pagecontrol?.addSubview(imageView)
            scrollview_pagecontrol?.isPagingEnabled = true
            
            
            scrollview_pagecontrol?.contentSize = CGSize(width: CGFloat((scrollview_pagecontrol?.frame.size.width)! * CGFloat( bannerImgArr.count)), height: CGFloat((scrollview_pagecontrol?.frame.size.height)!))
            Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(self.scrollingTimer), userInfo: nil, repeats: true)
    }
    }
    
        func scrollingTimer() {
            scrollview_pagecontrol = (view.viewWithTag(1) as? UIScrollView)
            pageControl = (view.viewWithTag(12) as? UIPageControl)
            let contentOffset: CGFloat = scrollview_pagecontrol!.contentOffset.x
            let nextPage = Int(contentOffset / (scrollview_pagecontrol?.frame.size.width)!) + 1
            if nextPage != bannerImgArr.count {
                scrollview_pagecontrol?.scrollRectToVisible(CGRect(x: CGFloat(CGFloat( nextPage) * (scrollview_pagecontrol?.frame.size.width)!), y: CGFloat(0), width: CGFloat((scrollview_pagecontrol?.frame.size.width)!), height: CGFloat((scrollview_pagecontrol?.frame.size.height)!)), animated: true)
                pageControl?.currentPage = nextPage
            }
            else {
                scrollview_pagecontrol?.scrollRectToVisible(CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat((scrollview_pagecontrol?.frame.size.width)!), height: CGFloat((scrollview_pagecontrol?.frame.size.height)!)), animated: true)
                pageControl?.currentPage = 0
            }
        }
    
    // MARK: - UIScrollView Delegate
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
//        var pageWidth: CGFloat = scrollview_pagecontrol!.frame.size.width
//        var page: Int = floor((scrollview_pagecontrol!.contentOffset.x - pageWidth / CGFloat(2)) / pageWidth) + CGFloat(1)
        
        let pageWidth: CGFloat = scrollview_pagecontrol!.frame.size.width
        let Page: CGFloat = floor((scrollview_pagecontrol!.contentOffset.x - CGFloat(pageWidth / 2)) / CGFloat(pageWidth)) + CGFloat(1)
        pageControl?.currentPage = Int(Page)
    }
    
    
    func getImageFromURL(_ fileURL: String) -> UIImage {
        var result: UIImage?
        let data = NSData(contentsOf: URL(string: fileURL)!)
        result = UIImage(data: data! as Data)
        return result!
    }
    @IBAction func btn_saveAction(_ sender: Any)
    {
        
        
      
        // create a name for your image
       // let fileURL = documentsDirectoryURL.URLByAppendingPathComponent("Image1.jpg")
        
        for i in 0...bannerImgArr.count-1
        {
            //  let dict = gallery[i] as? Dictionary<String,String>
            
            let image_url = bannerImgArr[i] as! String
            let image = getImageFromURL(image_url)

            let image_data = UIImagePNGRepresentation(image)
            let imageData = UIImage(data: image_data!)
            UIImageWriteToSavedPhotosAlbum(imageData!, nil, nil, nil)
        }
        
        self.displayAlertWithMessage(viewController:self , title: "Message", message: "Photos has been saved in your device")
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    
    // MARK:- Server call of getService Provider Method
    func callgetTourDetailInfoMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "category": category_id,
            "tour_id": tour_id,
            
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetTourDetailUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let success = result["success"] as! String
            
            
            
            if success == "true"
            {
                // parse data from server
                let tourDetail=result["data"] as? [[String: Any]]
                if tourDetail?.count != 0
                {
                self.parseGetTourListData(dataArray: tourDetail!)
                }
                else
                {
                    DispatchQueue.main.async
                        {
                             self.activityIndicatorView.stopAnimating()
                    }
                }
            }
            else
            {
                DispatchQueue.main.async
                    {
                        self.activityIndicatorView.stopAnimating()
                self.displayAlertWithMessage(viewController:self,title:"Alert" ,message:"")
                }
            }
            
            
        }
    }
    // MARK:- parseGetserviceProviderListData
    func parseGetTourListData(dataArray:[[String: Any]])
    {
        
//       var currency = String()
//         var price = String()
        
        
                DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                if let title = dataArray[0]["title"] as? String
                {
                    self.address = title
                    self.lbl_title.text = title
                }
             
                if let address = dataArray[0]["address"] as? String
                {
                    
                     self.lbl_address.text = address
                }
                if let description = dataArray[0]["description"] as? String
                {
                     self.lbl_description.text = description
                }
//                if let currency1 = dataArray[0]["curency"] as? String
//                {
//                    //currency = currency1
//                }
//                if let price1 = dataArray[0]["price"] as? String
//                {
//                   // price = price1
//                }
                if let lat = dataArray[0]["lat"] as? String
                {
                     self.lat = Double(lat)!
                }
                if let lng = dataArray[0]["lng"] as? String
                {
                    self.lng  = Double(lng)!
                }
                if let featureimage = dataArray[0]["feature_image"] as? String
                {
                    self.bannerImgArr.add(featureimage)
                    self.address_image = featureimage
                    
                }
              //  self.btn_price.setTitle(currency + " " + price, for: .normal)
                
                
//                   self.lbl_title.text = dataArray[0]["title"] as! String?
//                  self.lbl_address.text = dataArray[0]["address"] as! String?
//              self.address =   (dataArray[0]["address"] as! String?)!
//                let description = dataArray[0]["description"] as! String?
//               
//                self.lbl_description.text = description
//                
//                  let currency = dataArray[0]["curency"] as! String?
//                 let price = dataArray[0]["price"] as! String?
//                
//                self.btn_price.setTitle(currency! + " " + price!, for: .normal)
//                self.lat = Double(dataArray[0]["lat"] as! String)!
//                self.lng = Double(dataArray[0]["lng"] as! String)!
//                
//                self.bannerImgArr.add(dataArray[0]["feature_image"]!)
                
                if self.callingWay != "notification"
                {
               let gallery = dataArray[0]["gallery"] as AnyObject
                  
              if gallery.count != 0
                {
                    
                for i in 1...(gallery as AnyObject).count
                {
                  //  let dict = gallery[i] as? Dictionary<String,String>
                   
                    let image = gallery[String(i)] as! String
                    self.bannerImgArr.add(image)
                }
                    }
                    
                }
                self.createPageControl()
        }
        
    }
    @IBAction func btn_viewMapAction(_ sender: Any)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        vc.lat = lat
        vc.lng = lng
        vc.address = address
        vc.address_image = self.address_image
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_typeCategoriesAction(_ sender: Any)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension String {
    
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:String.Encoding.utf8], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
