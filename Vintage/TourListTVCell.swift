//
//  TourListTVCell.swift
//  Vintage
//
//  Created by chawtech solutions on 3/23/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class TourListTVCell: UITableViewCell {
    @IBOutlet weak var btn_showme : UIButton!
  //  @IBOutlet weak var view_back : UIView!
     @IBOutlet weak var imgv_image : UIImageView!
     @IBOutlet weak var lbl_title : UILabel!
     @IBOutlet weak var lbl_location : UILabel!
    // @IBOutlet weak var btn_showme : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.view_back.backgroundColor = UIColor.white
//        self.view_back.layer.cornerRadius = 5.0
//        // self.view_taxiBack.layer.borderWidth = 2.0
//        self.view_back.layer.borderColor = UIColor.clear.cgColor
//        self.view_back.layer.shadowColor = UIColor.lightGray.cgColor
//        self.view_back.layer.shadowOpacity = 1.0
//        self.view_back.layer.shadowRadius = 1.0
//        self.view_back.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(3))
        
        btn_showme.layer.cornerRadius = btn_showme.frame.size.height/2
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
