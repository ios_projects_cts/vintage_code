//
//  ViewController.swift
//  Vintage
//
//  Created by chawtech solutions on 3/22/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // TODO:- set navigation controller properties
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = false
        
        // TODO:- wait splash for 2 min after that call next controller
        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            
//            
//            let email = UserDefaults.standard.object(forKey:"email")as? String
//            
//            if email == nil
//            {
//                let storyboard = UIStoryboard(name:"Main", bundle: nil)
//                let vc  = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
//            else
//            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(vc, animated: false)
           // }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

