//
//  HomeViewController.swift
//  Vintage
//
//  Created by chawtech solutions on 3/22/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation


class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,CLLocationManagerDelegate {
    
    var locationManager:CLLocationManager!
    var current_latitude = Double()
    var current_longitude = Double()
     var address = String()
    var refreshControl: UIRefreshControl!
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var checkbox_btn = UIButton()
    var login_btn  = UIButton()
  
     @IBOutlet weak var view_scrollIndicator : UIView!

       @IBOutlet weak var btn_tour : UIButton!
       @IBOutlet weak var btn_settings : UIButton!

      @IBOutlet weak var view_setting : UIView!
      @IBOutlet weak var view_tour : UIView!
    
  
    @IBOutlet weak var imgv_setting : UIImageView!
    @IBOutlet weak var imgv_tour : UIImageView!
    
 
    @IBOutlet weak var lbl_setting : UILabel!
    @IBOutlet weak var lbl_tour : UILabel!
    let tourlistArr = NSMutableArray()
    var  scrollviewPaging = UIScrollView()
    var tableView: UITableView  =   UITableView()
    var paging = Int()
    var lastIndex = Int()
     var notification_status = String()
    var ProductCount = Int()
    var limit_prod_count = Int()
    var category = String()
   var  isMoreDataAvailable = Bool()
     var  iscategoryAvailable = Bool()

     var activityIndicatorView: ActivityIndicatorView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = true
   
        //Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlepushnotification(_:)), name: NSNotification.Name(rawValue: "notificationFire"), object: nil)
        
        NotificationCenter.default.addObserver(
            self,selector: #selector(registerFcmTokentoServer),
            name:NSNotification.Name(rawValue: "REGISTERFCMTOKEN"),object: nil)
        
    }
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(
            self,
            name:NSNotification.Name(rawValue:"REGISTERFCMTOKEN"),object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"notificationFire"), object: nil)
    }
    // handle notification
    func handlepushnotification(_ notification: NSNotification) {
        
        let data = notification.userInfo?["data"] as! [String:AnyObject]
        print(data)
        self.createAlertViewForPushNotification(data: data)
        
    }
    
    
    func createAlertViewForPushNotification(data:[String:AnyObject])
    {

        let id =  data["id"]as? String
        let tour_id = id?.replacingOccurrences(of:"\"", with:"")
        
        
        // let notification_type = String(describing:data["notificaton_type"])
      
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"TourDetailViewController") as! TourDetailViewController
                vc.tour_id = tour_id!
                vc.callingWay = "notification"
                self.navigationController?.pushViewController(vc, animated: false)
                //self.present(vc, animated: true, completion: nil)
            
            
        }
    }

    // handle notification
    func categoryprovider(_ notification: NSNotification)
    {
        paging = 0
         category = (notification.userInfo?["cat"] as? String)!
        tourlistArr.removeAllObjects()
         iscategoryAvailable = true
        self.callgetTourListMethod()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserDefaults.standard.object(forKey:"id") != nil
        {
          let value =  UserDefaults.standard.object(forKey:"notification_status") as! String
            
            if value == "1"
            {
                 checkbox_btn.setBackgroundImage(UIImage(named:"selected_checkbox"), for: UIControlState.normal)
            }
            else
            
            {
                checkbox_btn.setBackgroundImage(UIImage(named:"unselected_checkbox"), for: UIControlState.normal)
            }
           
           
        }
        else
        {
             checkbox_btn.setBackgroundImage(UIImage(named:"unselected_checkbox"), for: UIControlState.normal)
        }
        
        
        if UserDefaults.standard.object(forKey:"id") == nil
        {
            login_btn.setTitle("CLICK FOR LOGIN", for: .normal)
        }
        else
        {
            login_btn.setTitle("LOGOUT", for: .normal)
        }
        
        
    }
    func registerFcmTokentoServer(notification: NSNotification)
    {
        if let userInfo = notification.object as? [String:AnyObject] {
            let refreshToken = userInfo["refreshtoken"] as? String
            if refreshToken != nil {
                callupdateFcmTokenToServerMethod(refreshFcmToken:refreshToken!)
            }
            
        }
    }
    
    //MARK:-  server call of callupdateFcmTokenToServer Method
    func callupdateFcmTokenToServerMethod(refreshFcmToken:String)
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey:
                "id")!,
            "device":"ios",
            "fcm_key": refreshFcmToken,
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalupdateFcmTokensUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.isMoreDataAvailable = false
         iscategoryAvailable = false
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.categoryprovider(_:)), name: NSNotification.Name(rawValue: "categoryprovider"), object: nil)
 
        paging = 0
        category = ""
        scrollviewPaging = UIScrollView(frame: CGRect(x: 0, y: 260, width: self.view.frame.size.width, height: self.view.frame.size.height-192))
        scrollviewPaging.isPagingEnabled = true
        scrollviewPaging.delegate = self
       
        self.view.addSubview(scrollviewPaging)
        
        scrollviewPaging.contentSize = CGSize(width:self.view.frame.size.width,height:scrollviewPaging.frame.size.height)
        
     
        let rect = CGRect(x:0 , y: 83, width: scrollviewPaging.frame.size.width, height: scrollviewPaging.frame.size.height)
        scrollviewPaging.scrollRectToVisible(rect, animated: true)
        
     
        self.createUiTrendLinksButton()
       
        self.callgetTourListMethod()
      
        
        //refresh home page
        refreshControl = UIRefreshControl()
        tableView.addSubview(refreshControl)
        self.refreshControl.addTarget(self, action: #selector(self.refreshTableView), for: UIControlEvents.valueChanged )
       
        let notifBack = appDelegate.notifBack
        if notifBack == true
        {
            let data = appDelegate.UserInfo
            
            //let data = notification.userInfo?["data"]
            print(data)
            self.createAlertViewForPushNotification(data: data as! [String : AnyObject])
        }

    }
    
    func refreshTableView()
    {
        category = ""
        paging = 0
        tourlistArr.removeAllObjects()
        self.callgetTourListMethod()
    }
    
    
    
       func btn_getNotifiedAction(sender: UIButton!)
    {
        
        let instagramHooks = "instagram://user?username=ngs_sign_crew_london"
        let instagramUrl = NSURL(string: instagramHooks)
        if UIApplication.shared.canOpenURL(instagramUrl! as URL)
        {
           // UIApplication.shared.openURL(instagramUrl! as URL)
             UIApplication.shared.open(NSURL(string:instagramHooks)! as URL, options: [:], completionHandler: nil)
            
        } else {
            //redirect to safari because the user doesn't have Instagram
           // UIApplication.shared.openURL(NSURL(string: "http://instagram.com/")! as URL)
             UIApplication.shared.open(NSURL(string:"http://instagram.com/ngs_sign_crew_london")! as URL, options: [:], completionHandler: nil)
        }
    }
    
    
    func createUiTrendLinksButton()
    {
         let btn_typeCategory = UIButton()
         btn_typeCategory.frame = CGRect(x:
             20,y: 5, width:self.view.frame.size.width/2-30,height: 35)
        btn_typeCategory.setTitle("LONDON LETTERS", for: .normal)
        btn_typeCategory.setTitleColor(UIColor.white, for: .normal)
        btn_typeCategory.backgroundColor = UIColor.init(red: 254/255.0, green: 100/255.0, blue: 7/255.0, alpha: 1)
        btn_typeCategory.titleLabel?.font =  UIFont(name:"Gotham Bold", size: 12)
//        UIFont.familyNames.forEach {
//            print($0)
//            UIFont.fontNames(forFamilyName: $0).forEach {
//                print("  \($0)")
//            }
//        }
        
     
        btn_typeCategory.layer.cornerRadius = btn_typeCategory.frame.size.height/2
        btn_typeCategory.clipsToBounds = true
       
        btn_typeCategory.addTarget(self,action:#selector(btn_typeCategoryAction(sender:)), for: .touchUpInside)
        self.scrollviewPaging.addSubview(btn_typeCategory)
        
        let btn_viewMap = UIButton()
         btn_viewMap.frame = CGRect(x:btn_typeCategory.frame.size.width + 40,y: 5, width:self.view.frame.size.width/2-30,height: 35)
        btn_viewMap.setTitle("VIEW MAP", for: .normal)
        btn_viewMap.setTitleColor(UIColor.black, for: .normal)
        btn_viewMap.backgroundColor = UIColor.white
        btn_viewMap.layer.borderWidth = 1
        btn_viewMap.titleLabel?.font  = UIFont(name:"Gotham Bold", size: 12)
        btn_viewMap.layer.borderColor = UIColor.init(red: 254/255.0, green: 100/255.0, blue: 7/255.0, alpha: 1).cgColor
        btn_viewMap.layer.cornerRadius = btn_viewMap.frame.size.height/2
        btn_viewMap.clipsToBounds = true
        
       
        btn_viewMap.addTarget(self,action:#selector(btn_viewMapAction(sender:)), for: .touchUpInside)
        self.scrollviewPaging.addSubview(btn_viewMap)
        
        
        let btn_instagram = UIButton()
        btn_instagram.frame = CGRect(x:20,y: self.scrollviewPaging.frame.size.height-110, width:self.view.frame.size.width-40,height: 35)
        btn_instagram.setTitle("FOLLOW US ON INSTAGRAM", for: .normal)
        btn_instagram.setTitleColor(UIColor.white, for: .normal)
        btn_instagram.backgroundColor = UIColor.init(red: 62/255.0, green: 69/255.0, blue: 82/255.0, alpha: 1)
       // btn_instagram.layer.borderWidth = 1
        btn_instagram.titleLabel?.font  = UIFont(name:"Gotham Bold", size: 14)
        //btn_instagram.layer.borderColor = UIColor.init(red: 254/255.0, green: 100/255.0, blue: 7/255.0, alpha: 1).cgColor
        //btn_instagram.layer.cornerRadius = btn_viewMap.frame.size.height/2
        //btn_instagram.clipsToBounds = true
        
        
        btn_instagram.addTarget(self,action:#selector(btn_getNotifiedAction(sender:)), for: .touchUpInside)
        self.scrollviewPaging.addSubview(btn_instagram)
        
        tableView = UITableView(frame: (CGRect (x:0, y:50 , width: scrollviewPaging.frame.size.width, height: scrollviewPaging.frame.size.height-170)), style: UITableViewStyle.plain)
        tableView.delegate      =   self
        tableView.dataSource    =   self
        //tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName:"TourListTVCell",bundle: nil),forCellReuseIdentifier: "cell")
        // tableView.layer.borderColor = UIColor.lightGray.cgColor
        // tableView.layer.borderWidth = 0.5
        tableView.separatorStyle = .none
        scrollviewPaging.isScrollEnabled = true
        scrollviewPaging.addSubview(tableView)

       
    }
    
    func btn_typeCategoryAction(sender: UIButton!)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func btn_viewMapAction(sender: UIButton!)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        vc.address_image = "google"
        self.navigationController?.pushViewController(vc, animated: true)
    }



   
    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tourlistArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! TourListTVCell
        
        let dict = tourlistArr[indexPath.row] as? Dictionary<String,String>
        cell.backgroundColor = UIColor.white
        
        cell.lbl_title.text = dict?["title"]
        cell.lbl_location.text = dict?["location"]
        let imageString =  dict?["feature_image"]
         cell.btn_showme.tag = indexPath.row
       // cell.imgv_image.image = UIImage(named: "placeholder")
     
         DispatchQueue.main.async {
        let imgURL = URL(string:imageString!)
            cell.imgv_image.kf.setImage(with: imgURL)}
      
       
        cell.btn_showme.addTarget(self,action:#selector(buttonShowMeClicked(sender:)), for: .touchUpInside)
        

        
        return cell
        
    }

      func buttonShowMeClicked(sender:UIButton) {
           let dict = tourlistArr[sender.tag] as? Dictionary<String,String>
       
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "TourDetailViewController") as! TourDetailViewController
        vc.tour_id =  (dict?["id"])!
        vc.callingWay = "normal"
         vc.category_id =  (dict?["category_id"])!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
        {
            return 250
        }
   
    
   
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
                if(indexPath.row == (self.tourlistArr.count-1)) {
                if isMoreDataAvailable == true {
                     paging = paging + 1
                    self.callgetTourListMethod()
                }
            
            
        }
        
        
    }
    @IBAction func btn_viewMapAction(_ sender: Any)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = tourlistArr[indexPath.row] as? Dictionary<String,String>
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "TourDetailViewController") as! TourDetailViewController
        vc.tour_id =  (dict?["id"])!
        vc.callingWay = "normal"
        vc.category_id =  (dict?["category_id"])!
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    // MARK:- Server call of getService Provider Method
    func callgetTourListMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "category": category,
            "page": String(paging),
            
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetTourListUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let success = result["success"] as! String
            
            if success == "true"
            {
                // parse data from server
                let tourArr = result["data"] as? [[String: Any]]
                if tourArr?.count != 0
                {
                    self.parseGetTourListData(dataArray: tourArr!)
                    if tourArr?.count == 1
                    {
                    self.isMoreDataAvailable = false
                    }
                    else
                    {
                        self.isMoreDataAvailable = true
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        self.isMoreDataAvailable = false
                        self.tableView.reloadData()
                        self.activityIndicatorView.stopAnimating()
                        
                       if  self.iscategoryAvailable  == true
                       {
                        self.displayAlertWithMessage(viewController:self,title:"Message" ,message:"Loading more letters today")
                        }
                    }
                    
                }
            }
            else
            {
                DispatchQueue.main.async
                {
                self.activityIndicatorView.stopAnimating()
                self.displayAlertWithMessage(viewController:self,title:"Alert!" ,message:"Server error! Please try again")
                }
            }
            
            
        }
    }
     // MARK:- parseGetserviceProviderListData
    func parseGetTourListData(dataArray:[[String: Any]])
    {
        let tourDict = NSMutableDictionary()
        for i in 0...dataArray.count-1
        {
            tourDict.setValue((dataArray[i]["feature_image"] as? NSString), forKey: "feature_image")
            tourDict.setValue((dataArray[i]["category_id"] as? NSString), forKey: "category_id")
            tourDict.setValue((dataArray[i]["location"] as? NSString), forKey: "location")
            tourDict.setValue((dataArray[i]["title"] as? NSString), forKey: "title")
              tourDict.setValue((dataArray[i]["id"] as? NSString), forKey: "id")
            tourlistArr.add(tourDict .mutableCopy())
            
        }
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                self.tableView.reloadData()
                if self.refreshControl.isRefreshing == true {
                    self.refreshControl.endRefreshing()
                }
                
        }
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
