//
//  AppDelegate.swift
//  Vintage
//
//  Created by chawtech solutions on 3/22/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import IQKeyboardManager
import Firebase
import UserNotifications
import UserNotificationsUI




@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate {

    var window: UIWindow?
    var notifBack: Bool!
    
    var UserInfo = [AnyHashable: Any]()

    
    
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert,.badge,.sound])
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared().isEnabled = true
        let barAppearace = UIBarButtonItem.appearance()
        barAppearace.setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        //UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        UINavigationBar.appearance().tintColor = UIColor.white
             UINavigationBar.appearance().barTintColor = UIColor.init(red: 62/255.0, green: 69/255.0, blue: 82/255.0, alpha: 1)
       
        UINavigationBar.appearance().titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "Gotham Bold", size: 16)!,NSForegroundColorAttributeName:UIColor.white
        ]
        
        
        //TODO:- registered key for google map and google places api
        GMSServices.provideAPIKey("AIzaSyBQ3bL0OThcTrgW8kNMUun6i1ANhnHLBK4")
        
        
        //  Mark:- FCM Registration
        FIRApp.configure()
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
         
            
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        NotificationCenter.default.addObserver(self, selector:
            #selector(tokenRefreshNotification), name:
            NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        
        application.registerForRemoteNotifications()
        
        let token = FIRInstanceID.instanceID().token()
        print(token as Any)
        
    
        if launchOptions != nil {
            //opened from a push notification when the app is closed
            let userInfo: [AnyHashable: Any]? = (launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as?  [AnyHashable: Any])
            if userInfo != nil {
                
                notifBack = true
                UserInfo = userInfo!
            }
        }

        return true
    }
    
    
    // MARK: - Private Methods
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            
            completionHandler(success)
        }
    }
    
    
    private func scheduleLocalNotification() {
        // Create Notification Content
        let notificationContent = UNMutableNotificationContent()
        
        // Configure Notification Content
        notificationContent.title = "Cocoacasts"
        notificationContent.subtitle = "Local Notifications"
        notificationContent.body = "In this tutorial, you learn how to schedule local notifications with the User Notifications framework."
        
        // Add Trigger
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.2, repeats: false)
        
        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: "cocoacasts_local_notification", content: notificationContent, trigger: notificationTrigger)
        
        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if let error = error {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
            }
        }
    }
    


    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage)
    {
        
    }
    
    
    // MARK:- token Refresh Notification method for FCM
    func tokenRefreshNotification(_ notification: Notification) {
        let refreshedToken = FIRInstanceID.instanceID().token()
        //            print("InstanceID token: \(refreshedToken)")
        //
        //
        //
        //            UserDefaults.standard.set(refreshedToken, forKey:"fcm_token")
        //            UserDefaults.standard.synchronize()
        //        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        if (refreshedToken != nil)
        {
            
            let userid = UserDefaults.standard.object(forKey:"userid")as? String
            if userid != nil {
                
                if refreshedToken != UserDefaults.standard.object(forKey:"fcm_token") as? String
                {
                    
                    let userinfo: [String:AnyObject] = ["refreshtoken": refreshedToken! as AnyObject]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "REGISTERFCMTOKEN"), object:userinfo)
                    
                }
            }
            else
            {
                UserDefaults.standard.set(refreshedToken, forKey:"fcm_token")
                UserDefaults.standard.synchronize()
            }
        }
        print(refreshedToken as Any)
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    
    
    // MARK:- connectToFcm method for FCM
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    
//    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
//        
//    }
    
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // Let FCM know about the message for analytics etc.
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        // handle your message
    }
    
    
    // MARK:- didRegisterForRemoteNotificationsWithDeviceToken
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken as Data, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
    
    
    //MARK:- didReceiveRemoteNotification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //   if let messageID = userInfo[gcmMessageIDKey] {
        //      print("Message ID: \(messageID)")
        //  }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //  if let messageID = userInfo[gcmMessageIDKey] {
        //      print("Message ID: \(messageID)")
        //  }
        
        // Print full message.
        print(userInfo)
       // UNUserNotificationCenter.current().delegate = self
       // self.scheduleNotifications()
//        UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
//            switch notificationSettings.authorizationStatus {
//            case .notDetermined:
//                self.requestAuthorization(completionHandler: { (success) in
//                    guard success else { return }
//                    
//                    // Schedule Local Notification
//                    self.scheduleLocalNotification()
//                })
//            case .authorized:
//                // Schedule Local Notification
//                self.scheduleLocalNotification()
//            case .denied:
//                print("Application Not Allowed to Display Notifications")
//            }
//        }

        
        let DataDict:[String: AnyObject] = ["data": userInfo as AnyObject]
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:"notificationFire"), object: nil, userInfo: DataDict)
        
        
        //Register to receive notification in your class
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.handlepushnotificationAppDelegate(_:)), name: NSNotification.Name(rawValue: "notificationFire"), object: nil)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
  //  There is a delegate method to display the notification when the app is open in iOS 10. You have to implement this in order to get the rich notifications working when the app is open.
    
    
    
   
        
    
   
    
    func scheduleNotifications() {
        
        let content = UNMutableNotificationContent()
        let requestIdentifier = "rajanNotification"
        
        content.badge = 1
        content.title = "This is a rich notification"
        content.subtitle = "Hello there, I am Rajan Maheshwari"
        content.body = "Hello body"
        content.categoryIdentifier = "actionCategory"
        content.sound = UNNotificationSound.default()
        
//        // If you want to attach any image to show in local notification
//        let url = Bundle.main.url(forResource: "notificationImage", withExtension: ".jpg")
//        do {
//            let attachment = try? UNNotificationAttachment(identifier: requestIdentifier, url: url!, options: nil)
//            content.attachments = [attachment!]
//        }
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
        
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error:Error?) in
            
            if error != nil {
                print(error?.localizedDescription as Any)
            }     
            print("Notification Register Success")
        }
    }
       func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        connectToFcm()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

